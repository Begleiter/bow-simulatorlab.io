# Getting Involved

---

There are many ways to get involved in this project. The following sections will give you an overview, but feel free to [contact me](contact.md) with your own ideas as well. Any help is appreciated!

### Feedback, Feature Requests and Bug Reports

Tell me what you like and dislike about the program. Was it easy to get started with?
What do you use it for? Is there a specific feature you wish it had?
Also, be sure to report any bugs or other problems you find.

### Software Development

Bow Simulator is written in C++ with a Qt GUI.
There are always many things to implement or improve, from the user interface down to the simulation model and the numerical methods.
The source code is hosted at [https://gitlab.com/bow-simulator](https://gitlab.com/bow-simulator). <!--Check out the Readme for build instructions, an introduction to the codebase and how to contribute.-->
If you need ideas what to work on, have a look at the [issue tracker](https://gitlab.com/bow-simulator/bow-simulator/issues).
I will gladly help you getting started.

### Validation of Simulation Results

This is perhaps the most important topic currently.
If you have used this program to design a real bow, let me know how the simulation compares to reality. The draw curve of a bow for example is fairly easy to obtain.

Many other simulation results however are difficult to measure, think about material stresses for example.
Therefore someone with access to advanced measuring equipment (things strain gauges, acceleration sensors, a high speed camera, a tension testing machine, etc.) would be extremely helpful for this project.

### Documentation

Help to make sure the User Manual and/or the Technical Documentation are as complete and comprehensible as they can be.
These documents also need to be kept up to date with new versions of the software.
Someone with an outside perspective would be a great help here.

<!--All documentation is written in LaTex, the source files can be found at [https://bitbucket.org/stfnp/bow-simulator-docs](https://bitbucket.org/stfnp/bow-simulator-docs).-->

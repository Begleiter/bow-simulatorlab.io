# Mailing Lists

---

If you are interested in news and discussion about Bow Simulator you can subscribe to any of the mailing lists below.
(Of course you can unsubscribe later at any time if you change your mind.)

* **Announcements and news about upcoming versions**
  <br>[bow-simulator-announce](https://lists.sourceforge.net/lists/listinfo/bow-simulator-announce) ([archive](https://sourceforge.net/p/bow-simulator/mailman/bow-simulator-announce) | [search](https://sourceforge.net/p/bow-simulator/mailman/search/?mail_list=bow-simulator-announce))

* **General discussion and support**
  <br>[bow-simulator-discussion](https://lists.sourceforge.net/lists/listinfo/bow-simulator-discussion) ([archive](https://sourceforge.net/p/bow-simulator/mailman/bow-simulator-discussion) | [search](https://sourceforge.net/p/bow-simulator/mailman/search/?mail_list=bow-simulator-discussion))
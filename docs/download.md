# Download
---

### Windows

Installer for 32 bit and 64 bit versions of Windows. Uninstall any previous versions before running it.

<img src="../images/icon_msi.png" style="width: 25px; margin: 0px 10px 0px 0px">[bow_simulator-0.3-win32.exe](https://sourceforge.net/projects/bow-simulator/files/0.3.0/bow_simulator-0.3-win32.exe/download)
<br>
<br>

### Linux

Bow Simulator for Linux is either available as a deb package for debian based distributions or as a [snap package](http://snapcraft.io/), a self-contained package that works across most of the major linux distributions. To work with snap packages you need to have [snapd](http://snapcraft.io/docs/core/install) installed on your system. You can then install Bow Simulator by typing

```text
sudo snap install --dangerous bow_simulator-[...].snap
```
<img src="../images/icon_deb.png" style="width: 25px; margin: 0px 10px 0px 0px">[bow_simulator-0.3-linux64.deb](https://sourceforge.net/projects/bow-simulator/files/0.3.0/bow_simulator-0.3-linux64.deb/download)

<img src="../images/icon_snap.png" style="width: 25px; margin: 0px 10px 0px 0px">[bow_simulator-0.3-linux64.snap](https://sourceforge.net/projects/bow-simulator/files/0.3.0/bow_simulator-0.3-linux64.snap/download)

<!--
### Source Code

Building the program from source yourself should be possible for all [platforms supported by Qt](http://doc.qt.io/qt-5/supported-platforms.html).
Download the source code, unpack it and follow the instructions in the Readme.

[https://www.bitbucket.org/stfnp/bow-simulator](https://bitbucket.org/stfnp/bow-simulator).
-->

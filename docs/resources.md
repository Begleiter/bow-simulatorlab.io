# Resources

---

### User Manual

This is the main documentation for Bow Simulator. The User Manual gives you an overview of the program features, explains all input parameters and simulation results and also contains some background information.

<img src="../images/icon_pdf.png" style="width: 25px; margin: 0px 10px 0px 10px">[User Manual](files/user-manual.pdf)
<br><br>

<!--
### Technical Documentation

The Technical Documentation is geared towards developers and interested users who want to know exactly what the program is doing behind the scenes.
It contains all the theoretical work that Bow Simulator is based on, including the considerations that led to the mathematical bow model,
the derivation of the equations of motion and the numerical solution methods used to obtain the final results.

<img src="../img/icon_pdf.png" style="width: 25px; margin: 0px 10px 0px 10px">[Technical Documentation]()
<br>
<br>
-->

### Bow Design

Bow Simulator allows you to evaluate specific bow designs, but it doesn't really tell you how to design a good bow. If you want to learn about bow design you could have a look at the following sources:

* *The Traditional Bowyer's Bible*, Volume 1, Chapter 3: *Bow Design and Performance* by Tim Baker is probably the most complete work on this topic.
All the common design decisions and their performance implications are explained in a very practical and understandable way.

* The [article on bow design](http://crossbow.wikia.com/wiki/Bow_design) from the [crossbow building wiki](http://crossbow.wikia.com/wiki/Crossbow_Building_Wiki) is also a good read.
It is obviously geared towards crossbows but most of it also applies to bows in general.
<br><br>

### Similar Software

* [*SuperTiller*](http://www.buildyourownbow.com/build-alongs/how-to-use-supertiller-build-along/) by Alan Case is quite an impressive Excel spreadsheet that runs a complete static analysis of a user-defined bow.

* [*Woodbear's Selfbow Design Sheet [zip]*](files/woodbears-selfbow-design-sheet.zip) by David Dewey is also an Excel spreadsheet, but it uses a different approach: It calculates the necessary dimensions for a bow with a user-defined stress distribution. This is a very interesting way to design a bow.

* [*BowCAD*](https://www.indiegogo.com/projects/bowcad#/) is perhaps the only commercial attempt at bow design software so far. It started as an Indiegogo campaign and at one point they offered different kinds of licenses on their website. Unfortunately it seems to be gone now.

* [*Arrowmatcher*](http://www.x-ballistics.eu) Bow Simulator doesn't simulate any external ballistics. For this you might want to have a look at *Arrowmatcher*, a ballistics programs for crossbows. It can calculate arrow speed, energy and trajectory, among other things.

<!--
### Physics of Bow and Arrow

**Archery Physics**

* *Determining the Stiffness Properties of Bowstring Materials* by Stefan Pfeifer [(PFD)]()

* *A method for static dimensioning of bows* by Stefan Pfeifer [(PDF)]()
-->
